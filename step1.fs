namespace Sigmatism
open System

module Step1 =
    type Token = string
    type Node = 
        | NList of Node List
        | Symbol of string
        | Number of int

    module Reader = 
        open System.Text.RegularExpressions

        let tokenize (s : string) : Token List=
            let pattern = """[\s,]*(~@|[\[\]{}()'`~^@]|"(?:\\.|[^\\"])*"|;.*|[^\s\[\]{}('"`,;)]*)"""
            Regex.Matches(s, pattern)
            |> Seq.cast 
            |> Seq.map(fun (m : Match) -> m.Value.Trim())
            |> Seq.toList
        
        let rec readForm (t : Token List) : Node option * Token List =
            match t with
            | "(" :: xs -> readList [] xs
            | xs -> readAtom xs
        and readList (acc : Node List) (t : Token List) : Node option * Token List =
            match t with
            | ")" :: xs -> Some(NList(List.rev acc)), xs
            | tokens ->
                match readForm tokens with
                | Some(f), xs -> readList (f::acc) xs
                | _ -> System.Exception("Ån nej") |> raise

        and readAtom (t : Token List) : Node Option * Token List =                     
            match t with
            | x::xs -> Some(Symbol(x)), xs
            | _ -> System.Exception("OJOJ") |> raise


        let rec readForms (acc : Node List) (tokenList : Token List) : Node List = 
            match tokenList with
            | [] -> acc |> List.rev
            | tokens ->
                match readForm tokens with
                | Some(f), xs -> readForms (f::acc) xs
                | None, xs -> readForms acc xs

        let readString (input : string) : Node List =
            input |>  tokenize |> readForms []

    module Printer =
        let rec printHelper (n : Node) : unit = 
            match n with
            | NList n -> printList n
            | Symbol s -> printSymbol s
            | _ -> printfn "E"
        and printList (n : Node List) : unit =
            printf "("
            List.iter printHelper n
            printf ")"
        and printSymbol (s : string) : unit =
            printf "%s " s

        let printString (n : Node) : unit =
            printHelper n
            printf "\n"

    module Repl =
        let read (input : string) : Node List =
            Reader.readString input
        
        let rec eval (ast : Node) : Node =
            ast

        let print (n : Node) : unit =
            Printer.printString n |> ignore

        let repl (input : string) : unit =
            read input 
            |> Seq.map eval
            |> Seq.iter print