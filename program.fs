namespace Sigmatism
open Sigmatism.Step1.Repl
open System
module Program =
    [<EntryPoint>]
    let rec main args =
        Console.Write("user> ")
        Console.ReadLine() |> repl
        //"(1 (2 3))" |> repl
        main args