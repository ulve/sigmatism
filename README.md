# README

En implementation av [MAL](https://github.com/kanaka/mal) dvs en enkel LISP-implementation.

Målet med det här projektet är att använda så få snygga trick som möjligt och göra det hela mer lättläst

## Starta

* `git clone https://bitbucket.org/ulve/sigmatism.git`
* `dotnet resore`
* `dotnet run`

Kräver dotnetcore 1.0.4 eller senare