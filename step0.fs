namespace Sigmatism
open System

module Step0 =
    type Ast = string List

    module Reader = 
        let readString (s : string) : Ast =
            [s]

    module Repl =
        let read input =
            Reader.readString input

        let eval (ast : string) : string =
            ast

        let print (v : string) =
            printfn "%A" v
            
        let repl input =
            read input 
            |> Seq.ofList
            |> Seq.map eval
            |> Seq.iter print